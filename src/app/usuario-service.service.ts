import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

const ENDPOINT = 'https://api.github.com/users';

@Injectable()
export class UsuarioService {

    constructor(protected http: HttpClient) {
    }

    private static getUrlRecurso(): string {
        return ENDPOINT;
    }

    buscar(usuario: string): Observable<any> {
        return this.http.get<Response>(`${UsuarioService.getUrlRecurso()}/${usuario}`);
    }

    buscarRepositorios(repos: string): Observable<any> {
        return this.http.get<Response>(repos);
    }

}
