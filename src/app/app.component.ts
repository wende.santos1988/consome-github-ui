import {Component} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {UsuarioService} from './usuario-service.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'consome-github-ui';

    public form: FormGroup = null;
    public user: any = null;
    public repos: [] = null;

    constructor(private formBuider: FormBuilder,
                private service: UsuarioService) {
        this.form = this.criarFormulario();
    }

    protected criarFormulario(): FormGroup {
        return this.formBuider.group({
            search: [null],
        });
    }

    public buscarUsuario(): void {
        this.user = null;
        this.repos = null;
        this.service.buscar(this.form.value.search).subscribe(
            res => {
                console.log(res);
                this.user = res;
            }, error => {
                console.log(error);
            }
        );
    }

    public buscarRepositorios(repos: string): void {
        this.service.buscarRepositorios(repos).subscribe(
            res => {
                console.log(res);
                this.repos = res;
            }, error => {
                console.log(error);
            }
        )
    }

}
